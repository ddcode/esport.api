"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseUrlSlugs = require("mongoose-url-slugs");

var _mongooseUrlSlugs2 = _interopRequireDefault(_mongooseUrlSlugs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Tournament = _mongoose2.default.Schema({
  logo: String,
  name: String,
  bio: String,
  date_start: String,
  date_end: String
}, {
  timestamps: true
});

Tournament.plugin((0, _mongooseUrlSlugs2.default)("name", {
  field: "slug",
  update: true
}));

exports.default = _mongoose2.default.model("Tournament", Tournament);