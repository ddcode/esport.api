"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _teamsController = require("../controller/teamsController");

var _teamsController2 = _interopRequireDefault(_teamsController);

var _errors = require("../middlewares/errors");

var _multer = require("multer");

var _multer2 = _interopRequireDefault(_multer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var storage = _multer2.default.diskStorage({
  destination: function destination(req, file, cb) {
    cb(null, "./images/logos/");
  },

  filename: function filename(req, file, cb) {
    cb(null, file.originalname);
  }
});

var upload = (0, _multer2.default)({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 2
  }
});

exports.default = function () {
  var api = (0, _express.Router)();

  api.get("/", (0, _errors.catchAsync)(_teamsController2.default.findAll));
  api.get("/:slug", (0, _errors.catchAsync)(_teamsController2.default.findOne));

  api.post("/", upload.single("logo"), (0, _errors.catchAsync)(_teamsController2.default.create));

  api.put("/:slug", upload.single("logo"), (0, _errors.catchAsync)(_teamsController2.default.update));

  api.delete("/:slug", (0, _errors.catchAsync)(_teamsController2.default.remove));

  return api;
};