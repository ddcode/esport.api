"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tournament = require("../models/tournament");

var _tournament2 = _interopRequireDefault(_tournament);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  findOne: function findOne(req, res, next) {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      var tournament;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _tournament2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              tournament = _context.sent;

              if (tournament) {
                _context.next = 5;
                break;
              }

              return _context.abrupt("return", next());

            case 5:
              return _context.abrupt("return", res.status(200).send({
                data: tournament
              }));

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, _this);
    }))();
  },
  findAll: function findAll(req, res) {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
      var tournament;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _tournament2.default.find().sort({
                date_start: "desc"
              });

            case 2:
              tournament = _context2.sent;

              if (tournament) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", next);

            case 5:
              return _context2.abrupt("return", res.status(200).send({
                data: tournament
              }));

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }))();
  },
  create: function create(req, res) {
    var _this3 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
      var logo, tournament;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              logo = process.env.HOST + process.env.DEFAULT_LOGO;


              if (req.file) {
                logo = process.env.HOST + "/" + req.file.path;
              }

              _context3.next = 4;
              return new _tournament2.default({
                name: req.body.name,
                bio: req.body.bio,
                logo: logo,
                date_start: req.body.date_start,
                date_end: req.body.date_end
              }).save();

            case 4:
              tournament = _context3.sent;
              return _context3.abrupt("return", res.status(201).send({
                data: tournament,
                message: "New tournament added to database!"
              }));

            case 6:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, _this3);
    }))();
  },
  update: function update(req, res, next) {
    var _this4 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
      var tournament;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return _tournament2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              tournament = _context4.sent;

              if (tournament) {
                _context4.next = 5;
                break;
              }

              return _context4.abrupt("return", next());

            case 5:

              tournament.name = req.body.name;
              tournament.bio = req.body.bio;
              tournament.date_start = req.body.date_start;
              tournament.date_end = req.body.date_end;

              if (req.file) {
                tournament.logo = process.env.HOST + "/" + req.file.path;
              }

              _context4.next = 12;
              return tournament.save();

            case 12:
              return _context4.abrupt("return", res.status(200).send({
                data: tournament,
                message: "tournament " + tournament.name + " was updated!"
              }));

            case 13:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, _this4);
    }))();
  },
  remove: function remove(req, res, next) {
    var _this5 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
      var tournament;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return _tournament2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              tournament = _context5.sent;

              if (tournament) {
                _context5.next = 5;
                break;
              }

              return _context5.abrupt("return", next());

            case 5:

              tournament.remove();

              return _context5.abrupt("return", res.status(200).send({
                message: "tournament " + tournament.name + " was removed properly!"
              }));

            case 7:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, _this5);
    }))();
  }
};