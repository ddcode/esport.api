"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _match = require("../models/match");

var _match2 = _interopRequireDefault(_match);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  findOne: function findOne(req, res, next) {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      var match;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _match2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              match = _context.sent;

              if (match) {
                _context.next = 5;
                break;
              }

              return _context.abrupt("return", next());

            case 5:
              return _context.abrupt("return", res.status(200).send({
                data: match
              }));

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, _this);
    }))();
  },
  findAll: function findAll(req, res) {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
      var matches;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _match2.default.find().sort({
                createdAt: "desc"
              });

            case 2:
              matches = _context2.sent;

              if (matches) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", next);

            case 5:
              return _context2.abrupt("return", res.status(200).send({
                data: matches
              }));

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }))();
  },
  create: function create(req, res) {
    var _this3 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
      var match;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return new _match2.default({
                team_1: req.body.team_1,
                team_2: req.body.team_2,
                date: req.body.date,
                event: req.body.event
              }).save();

            case 2:
              match = _context3.sent;
              return _context3.abrupt("return", res.status(201).send({
                data: match,
                message: "New match added to database!"
              }));

            case 4:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, _this3);
    }))();
  },
  update: function update(req, res, next) {
    var _this4 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
      var match;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return _match2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              match = _context4.sent;

              if (match) {
                _context4.next = 5;
                break;
              }

              return _context4.abrupt("return", next());

            case 5:

              match.team_1 = req.body.team_1;
              match.team_2 = req.body.team_2;
              match.date = req.body.date;
              match.event = req.body.event;

              _context4.next = 11;
              return match.save();

            case 11:
              return _context4.abrupt("return", res.status(200).send({
                data: match,
                message: "Match " + match.team_1 + " vs " + match.team_2 + " was updated!"
              }));

            case 12:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, _this4);
    }))();
  },
  remove: function remove(req, res, next) {
    var _this5 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
      var match;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return _match2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              match = _context5.sent;

              if (match) {
                _context5.next = 5;
                break;
              }

              return _context5.abrupt("return", next());

            case 5:

              match.remove();

              return _context5.abrupt("return", res.status(200).send({
                message: "match " + match.name + " was removed properly!"
              }));

            case 7:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, _this5);
    }))();
  }
};