"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _player = require("../models/player");

var _player2 = _interopRequireDefault(_player);

var _team = require("../models/team");

var _team2 = _interopRequireDefault(_team);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  findOne: function findOne(req, res, next) {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      var player;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _player2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              player = _context.sent;

              if (player) {
                _context.next = 5;
                break;
              }

              return _context.abrupt("return", next());

            case 5:
              return _context.abrupt("return", res.status(200).send({
                data: player
              }));

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, _this);
    }))();
  },
  findAll: function findAll(req, res) {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
      var players;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _player2.default.find().sort({
                createdAt: "desc"
              });

            case 2:
              players = _context2.sent;

              if (players) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", next);

            case 5:
              return _context2.abrupt("return", res.status(200).send({
                data: players
              }));

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }))();
  },
  create: function create(req, res) {
    var _this3 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
      var photo, _req$body, name, birthday, bio, country, team, teamData, player;

      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              photo = process.env.HOST + process.env.DEFAULT_PHOTO;


              if (req.file) {
                photo = process.env.HOST + "/" + req.file.path;
              }

              _req$body = req.body, name = _req$body.name, birthday = _req$body.birthday, bio = _req$body.bio, country = _req$body.country, team = _req$body.team;
              _context3.next = 5;
              return _team2.default.find({
                _id: team
              });

            case 5:
              teamData = _context3.sent;
              _context3.next = 8;
              return new _player2.default({
                photo: photo,
                name: name,
                birthday: birthday,
                country: country,
                bio: bio,
                team: teamData[0]
              }).save();

            case 8:
              player = _context3.sent;
              return _context3.abrupt("return", res.status(201).send({
                data: player,
                message: "New player added to database!"
              }));

            case 10:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, _this3);
    }))();
  },
  update: function update(req, res, next) {
    var _this4 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
      var player, _req$body2, name, birthday, bio, country, team, teamData;

      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return _player2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              player = _context4.sent;

              if (player) {
                _context4.next = 5;
                break;
              }

              return _context4.abrupt("return", next());

            case 5:
              _req$body2 = req.body, name = _req$body2.name, birthday = _req$body2.birthday, bio = _req$body2.bio, country = _req$body2.country, team = _req$body2.team;


              player.name = name;
              player.birthday = birthday;
              player.bio = bio;
              player.country = country;

              if (!team) {
                _context4.next = 15;
                break;
              }

              _context4.next = 13;
              return _team2.default.find({
                _id: team
              });

            case 13:
              teamData = _context4.sent;


              player.team = teamData[0];

            case 15:

              if (req.file) {
                player.photo = process.env.HOST + "/" + req.file.path;
              }

              _context4.next = 18;
              return player.save();

            case 18:
              return _context4.abrupt("return", res.status(200).send({
                data: player,
                message: "Player " + player.name + " was updated!"
              }));

            case 19:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, _this4);
    }))();
  },
  remove: function remove(req, res, next) {
    var _this5 = this;

    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
      var player;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return _player2.default.findOne({
                slug: req.params.slug
              });

            case 2:
              player = _context5.sent;

              if (player) {
                _context5.next = 5;
                break;
              }

              return _context5.abrupt("return", next());

            case 5:

              player.remove();

              return _context5.abrupt("return", res.status(200).send({
                message: "Player " + player.name + " was removed properly!"
              }));

            case 7:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, _this5);
    }))();
  }
};