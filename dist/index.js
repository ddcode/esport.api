"use strict";

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _path = require("path");

var _config = require("./config/config");

var _config2 = _interopRequireDefault(_config);

var _errors = require("./middlewares/errors");

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _players = require("./routes/players");

var _players2 = _interopRequireDefault(_players);

var _teams = require("./routes/teams");

var _teams2 = _interopRequireDefault(_teams);

var _matches = require("./routes/matches");

var _matches2 = _interopRequireDefault(_matches);

var _tournaments = require("./routes/tournaments");

var _tournaments2 = _interopRequireDefault(_tournaments);

var _database = require("./config/database");

var _database2 = _interopRequireDefault(_database);

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _dotenv = require("dotenv");

var _dotenv2 = _interopRequireDefault(_dotenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Import .env config
 */
_dotenv2.default.config();

// Custom


// Connect to database


// Custom Routes


_mongoose2.default.connect(_database2.default.mongoUrl);
_mongoose2.default.Promise = global.Promise;
_mongoose2.default.connection.on("error", function (err) {
  console.log("Could not connect to the database. Exiting now...");
  process.exit();
});

var app = (0, _express2.default)();

app.set("view engine", "pug");
app.set("views", (0, _path.join)(__dirname, "views"));
app.use(_express2.default.static("public"));
app.use(_bodyParser2.default.urlencoded({
  extended: false
}));
app.use(_bodyParser2.default.json());
app.use("/images", _express2.default.static("images"));

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");

  // Request headers you wish to allow
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

// routes config
app.use("/players", (0, _players2.default)());
app.use("/teams", (0, _teams2.default)());
app.use("/matches", (0, _matches2.default)());
app.use("/tournaments", (0, _tournaments2.default)());

// errors handling
app.use(_errors.notFound);
app.use(_errors.catchErrors);

// let's play!
app.listen(_config2.default.server.port, function () {
  console.log("Server is up!");
});