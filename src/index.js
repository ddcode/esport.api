import express from "express";
import {
  join
} from "path";
import config from "./config/config";
import {
  catchErrors,
  notFound
} from "./middlewares/errors";
import bodyParser from "body-parser";

// Custom Routes
import players from "./routes/players";
import teams from "./routes/teams";
import matches from "./routes/matches";
import tournaments from "./routes/tournaments";

// Connect to database
import dbConfig from "./config/database";
import mongoose from "mongoose";

// Custom
import dotenv from "dotenv";

/**
 * Import .env config
 */
dotenv.config();

mongoose.connect(dbConfig.mongoUrl);
mongoose.Promise = global.Promise;
mongoose.connection.on("error", err => {
  console.log("Could not connect to the database. Exiting now...");
  process.exit();
});

const app = express();

app.set("view engine", "pug");
app.set("views", join(__dirname, "views"));
app.use(express.static("public"));
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use("/images", express.static("images"));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

// routes config
app.use("/players", players());
app.use("/teams", teams());
app.use("/matches", matches());
app.use("/tournaments", tournaments());

// errors handling
app.use(notFound);
app.use(catchErrors);

// let's play!
app.listen(config.server.port, () => {
  console.log(`Server is up!`);
});