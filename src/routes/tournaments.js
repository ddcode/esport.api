import {
  Router
} from "express";
import tournamentsController from "../controller/tournamentsController";
import {
  catchAsync
} from "../middlewares/errors";
import multer from "multer";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./images/logos/");
  },

  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 2
  }
});

export default () => {
  const api = Router();

  api.get("/", catchAsync(tournamentsController.findAll));
  api.get("/:slug", catchAsync(tournamentsController.findOne));

  api.post("/", upload.single("logo"), catchAsync(tournamentsController.create));

  api.put("/:slug", upload.single("logo"), catchAsync(tournamentsController.update));

  api.delete("/:slug", catchAsync(tournamentsController.remove));

  return api;
};