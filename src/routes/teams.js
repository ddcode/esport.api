import {
  Router
} from "express";
import teamsController from "../controller/teamsController";
import {
  catchAsync
} from "../middlewares/errors";
import multer from "multer";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./images/logos/");
  },

  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 2
  }
});

export default () => {
  const api = Router();

  api.get("/", catchAsync(teamsController.findAll));
  api.get("/:slug", catchAsync(teamsController.findOne));

  api.post("/", upload.single("logo"), catchAsync(teamsController.create));

  api.put("/:slug", upload.single("logo"), catchAsync(teamsController.update));

  api.delete("/:slug", catchAsync(teamsController.remove));

  // Remove single player from the team
  api.delete('/:slug/player', catchAsync(teamsController.removePlayer));

  return api;
};