import {
  Router
} from "express";
import matchesController from "../controller/matchesController";
import {
  catchAsync
} from "../middlewares/errors";

export default () => {
  const api = Router();

  api.get("/", catchAsync(matchesController.findAll));
  api.get("/:slug", catchAsync(matchesController.findOne));

  api.post("/", catchAsync(matchesController.create));

  api.put("/:slug", catchAsync(matchesController.update));

  api.delete("/:slug", catchAsync(matchesController.remove));

  return api;
};