import { Router } from "express";
import playersController from "../controller/playersController";
import { catchAsync } from "../middlewares/errors";
import multer from "multer";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./images/photos/");
  },

  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 2
  }
});

export default () => {
  const api = Router();

  api.get("/", catchAsync(playersController.findAll));
  api.get("/:slug", catchAsync(playersController.findOne));

  api.post("/", upload.single("photo"), catchAsync(playersController.create));

  api.put(
    "/:slug",
    upload.single("photo"),
    catchAsync(playersController.update)
  );

  api.delete("/:slug", catchAsync(playersController.remove));

  return api;
};
