import Team from "../models/team";

export default {
  async findOne(req, res, next) {
    const team = await Team.findOne({
      slug: req.params.slug
    }).populate('players');
    if (!team) return next();

    return res.status(200).send({
      data: team
    });
  },

  async findAll(req, res) {
    const team = await Team.find().populate('players').sort({
      createdAt: "desc"
    });
    if (!team) return next;

    return res.status(200).send({
      data: team
    });
  },

  async create(req, res) {
    let logo = process.env.HOST + process.env.DEFAULT_LOGO;

    if (req.file) {
      logo = process.env.HOST + "/" + req.file.path;
    }

    const team = await new Team({
      name: req.body.name,
      bio: req.body.bio,
      logo: logo
    }).save();

    return res.status(201).send({
      data: team,
      message: "New team added to database!"
    });
  },

  async update(req, res, next) {
    let team = await Team.findOne({
      slug: req.params.slug
    });
    if (!team) return next();

    team.bio = req.body.bio;
    team.name = req.body.name;

    if (req.file) {
      team.logo = process.env.HOST + "/" + req.file.path;
    }

    await team.save();

    return res.status(200).send({
      data: team,
      message: `Team ${team.name} was updated!`
    });
  },

  async remove(req, res, next) {
    const team = await Team.findOne({
      slug: req.params.slug
    });
    if (!team) return next();

    team.remove();

    return res.status(200).send({
      message: `Team ${team.name} was removed properly!`
    });
  },

  async removePlayer(req, res, next) {
    const team = await Team.findOne({
      slug: req.params.slug
    });

    const player = req.body.id;

    if (!player) return nex();
    if (!team) return next();

    const index = team.players.indexOf(player);

    team.players.splice(index, 1);
    team.save();

    return res.status(200).send({
      message: `Player was removed properly!`
    });
  }
};