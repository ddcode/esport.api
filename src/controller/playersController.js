import Player from "../models/player";
import Team from "../models/team";

export default {
  async findOne(req, res, next) {
    const player = await Player.findOne({
      slug: req.params.slug
    }).populate('team');
    if (!player) return next();

    return res.status(200).send({
      data: player
    });
  },

  async findAll(req, res) {
    const players = await Player.find().populate('team').sort({
      createdAt: "desc"
    });
    if (!players) return next;

    return res.status(200).send({
      data: players
    });
  },

  async create(req, res) {
    let photo = process.env.HOST + process.env.DEFAULT_PHOTO;

    if (req.file) {
      photo = process.env.HOST + "/" + req.file.path;
    }

    const {
      name,
      birthday,
      bio,
      country,
      team
    } = req.body;

    const player = await new Player({
      photo: photo,
      name: name,
      birthday: birthday,
      country: country,
      bio: bio,
      team: team
    }).save();

    if (team) {
      player.team = team;
      const id = player._id;

      const teams = await Team.findOne({
        _id: team
      });

      teams.players.push(id);
      teams.save();
    }

    return res.status(201).send({
      data: player,
      message: "New player added to database!"
    });
  },

  async update(req, res, next) {
    const player = await Player.findOne({
      slug: req.params.slug
    });
    if (!player) return next();

    const {
      name,
      birthday,
      bio,
      country,
      team,
      removeTeam
    } = req.body;

    player.name = name;
    player.birthday = birthday;
    player.bio = bio;
    player.country = country;

    if (team) {
      const teams = await Team.findOne({
        _id: team
      });
      const id = player._id;

      if (removeTeam) {
        const index = teams.players.indexOf(id)

        player.team = null;
        teams.players.splice(index, 1);
      } else {
        player.team = team;
        teams.players.push(id);
      }

      teams.save();
    }

    if (req.file) {
      player.photo = process.env.HOST + "/" + req.file.path;
    }

    await player.save();

    return res.status(200).send({
      data: player,
      message: `Player ${player.name} was updated!`
    });
  },

  async remove(req, res, next) {
    const player = await Player.findOne({
      slug: req.params.slug
    });
    if (!player) return next();

    player.remove();

    return res.status(200).send({
      message: `Player ${player.name} was removed properly!`
    });
  }
};