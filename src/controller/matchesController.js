import Match from "../models/match";

export default {
  async findOne(req, res, next) {
    const match = await Match.findOne({
      slug: req.params.slug
    }).populate('teams');
    if (!match) return next();

    return res.status(200).send({
      data: match
    });
  },

  async findAll(req, res) {
    const matches = await Match.find().populate('teams').sort({
      createdAt: "desc"
    });
    if (!matches) return next;

    return res.status(200).send({
      data: matches
    });
  },

  async create(req, res) {
    // Manage Teams
    let teams = [];
    teams.push(req.body.team_1.value);
    teams.push(req.body.team_2.value);

    const match = await new Match({
      teams: teams,
      date: req.body.date,
      event: req.body.event
    }).save();

    return res.status(201).send({
      data: match,
      message: "New match added to database!"
    });
  },

  async update(req, res, next) {
    const match = await Match.findOne({
      slug: req.params.slug
    });
    if (!match) return next();

    match.team_1 = req.body.team_1;
    match.team_2 = req.body.team_2;
    match.date = req.body.date;
    match.event = req.body.event;

    await match.save();

    return res.status(200).send({
      data: match,
      message: `Match ${match.team_1} vs ${match.team_2} was updated!`
    });
  },

  async remove(req, res, next) {
    const match = await Match.findOne({
      slug: req.params.slug
    });

    if (!match) return next();

    match.remove();

    return res.status(200).send({
      message: `Match ${match.slug} was removed properly!`
    });
  }
};