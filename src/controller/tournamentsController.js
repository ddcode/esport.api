import Tournament from "../models/tournament";

export default {
  async findOne(req, res, next) {
    const tournament = await Tournament.findOne({
      slug: req.params.slug
    });
    if (!tournament) return next();

    return res.status(200).send({
      data: tournament
    });
  },

  async findAll(req, res) {
    const tournament = await Tournament.find().sort({
      date_start: "desc"
    });
    if (!tournament) return next;

    return res.status(200).send({
      data: tournament
    });
  },

  async create(req, res) {
    let logo = process.env.HOST + process.env.DEFAULT_LOGO;

    if (req.file) {
      logo = process.env.HOST + "/" + req.file.path;
    }

    const tournament = await new Tournament({
      name: req.body.name,
      bio: req.body.bio,
      logo: logo,
      date_start: req.body.date_start,
      date_end: req.body.date_end
    }).save();

    return res.status(201).send({
      data: tournament,
      message: "New tournament added to database!"
    });
  },

  async update(req, res, next) {
    const tournament = await Tournament.findOne({
      slug: req.params.slug
    });
    if (!tournament) return next();

    tournament.name = req.body.name;
    tournament.bio = req.body.bio;
    tournament.date_start = req.body.date_start;
    tournament.date_end = req.body.date_end;

    if (req.file) {
      tournament.logo = process.env.HOST + "/" + req.file.path;
    }

    await tournament.save();

    return res.status(200).send({
      data: tournament,
      message: `tournament ${tournament.name} was updated!`
    });
  },

  async remove(req, res, next) {
    const tournament = await Tournament.findOne({
      slug: req.params.slug
    });
    if (!tournament) return next();

    tournament.remove();

    return res.status(200).send({
      message: `tournament ${tournament.name} was removed properly!`
    });
  }
};