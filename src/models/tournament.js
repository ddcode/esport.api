import mongoose from "mongoose";
import URLSlugs from "mongoose-url-slugs";

const Tournament = mongoose.Schema({
  logo: String,
  name: String,
  bio: String,
  date_start: String,
  date_end: String
}, {
  timestamps: true
});

Tournament.plugin(
  URLSlugs("name", {
    field: "slug",
    update: true
  })
);

export default mongoose.model("Tournament", Tournament);