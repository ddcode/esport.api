import mongoose from "mongoose";
import URLSlugs from "mongoose-url-slugs";

const Match = mongoose.Schema({
  date: String,
  event: String,
  teams: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team'
  }]
}, {
  timestamps: true
});

Match.plugin(
  URLSlugs("id", {
    field: "slug",
    update: true
  })
);

export default mongoose.model("Match", Match);