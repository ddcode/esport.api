import mongoose from "mongoose";
import URLSlugs from "mongoose-url-slugs";

const Player = mongoose.Schema({
  name: String,
  birthday: String,
  country: String,
  bio: String,
  photo: String,
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team'
  }
}, {
  timestamps: true
});

Player.plugin(
  URLSlugs("name", {
    field: "slug",
    update: true
  })
);

export default mongoose.model("Player", Player);