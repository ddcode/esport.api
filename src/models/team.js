import mongoose from "mongoose";
import URLSlugs from "mongoose-url-slugs";

const Team = mongoose.Schema({
  logo: String,
  name: String,
  bio: String,
  players: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Player'
  }]
}, {
  timestamps: true
});

Team.plugin(
  URLSlugs("name", {
    field: "slug",
    update: true
  })
);

export default mongoose.model("Team", Team);